package me.custodio.Veever.enums;

/**
 * Created by Andrews on 17,May,2019
 */

public enum GeoDirections {
    NORTH,
    EAST,
    SOUTH,
    WEST,
    NORTH_EAST,
    SOUTH_EAST,
    SOUTH_WEST,
    NORTH_WEST,
    NO_DIRECTION
}
